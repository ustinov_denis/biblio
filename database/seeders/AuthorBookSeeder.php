<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AuthorBookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $count = 10;
        $i = 0;

        while ($i < $count) {
            DB::table('author')->insert([
                'fio' => 'writer' . $i,
            ]);

            DB::table('book')->insert([
                'name' => 'book_' . $i,
                'year' => rand(2000, 2024),
                'description' => 'description_' . $i,
                'isbn' => rand(1000000000, 9999999999999),
                'photo' => null,
            ]);

            $i++;
        }

        DB::table('author_book')->insert([
            'author_id' => 1,
            'book_id' => 1,
        ]);
    }
}
