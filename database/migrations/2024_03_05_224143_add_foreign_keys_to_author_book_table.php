<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('author_book', function (Blueprint $table) {
            $table->foreign(['author_id'], 'author_book_author_FK')->references(['id'])->on('author')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['book_id'], 'author_book_book_FK')->references(['id'])->on('book')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('author_book', function (Blueprint $table) {
            $table->dropForeign('author_book_author_FK');
            $table->dropForeign('author_book_book_FK');
        });
    }
};
