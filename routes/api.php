<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Enums\ResponseCode;
use App\Http\Controllers\AuthorController;
use App\Http\Controllers\BookController;
use App\Http\Controllers\MailingController;
use App\Http\Middleware\ApiRequestLogging;
use App\Http\Middleware\CheckTokenIsValid;
use App\Http\Resources\AuthorResource;
use App\Http\Resources\BookResource;
use App\Http\Resources\MailingResource;
use App\Models\Author;
use App\Models\Book;
use App\Models\Mailing;
use Illuminate\Support\Facades\Cache;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
    Route::middleware([CheckTokenIsValid::class])->group(function () {
        /*
         * /books
         */
        Route::post('/books', [BookController::class, 'store'])->middleware([ApiRequestLogging::class]);
        Route::put('/books/{id}', [BookController::class, 'update'])->middleware([ApiRequestLogging::class]);
        Route::delete('/books/{id}', [BookController::class, 'destroy'])->middleware([ApiRequestLogging::class]);

        /*
         * /authors
         */
        Route::post('/authors', [AuthorController::class, 'store'])->middleware([ApiRequestLogging::class]);
        Route::put('/authors/{id}', [AuthorController::class, 'update'])->middleware([ApiRequestLogging::class]);
        Route::delete('/authors/{id}', [AuthorController::class, 'destroy'])->middleware([ApiRequestLogging::class]);

        /*
         * /mailing
         */
        Route::post('/mailing', [MailingController::class, 'store'])->middleware([ApiRequestLogging::class]);
        Route::delete('/mailing/{id}', [MailingController::class, 'destroy'])->middleware([ApiRequestLogging::class]);

        Route::get('/mailing/{mail}', function (string $mail, Mailing $mailing) {
            $mailItem = $mailing->getMail($mail);
            if (!$mailItem) {
                return response(null, ResponseCode::NO_FOUND->value);
            }
            return new MailingResource($mailItem);
        })->middleware([ApiRequestLogging::class]);

        Route::get('/mailing', function (Request $request, Mailing $mailing) {
            $mails = Cache::remember('mailing_m' . ($request->get('page') ?? 1), config('app.cache_ttl'), function () use ($mailing) {
                return $mailing->getAllMails()->paginate(config('app.page_size'));
            });
            if (!$mails || count($mails) == 0) {
                return response(null, ResponseCode::NO_FOUND->value);
            }
            return MailingResource::collection($mails);
        })->middleware([ApiRequestLogging::class]);
    });

    /*
     * /books
     */
    Route::get('/books/{id}', function (int $id) {
        return new BookResource(Cache::remember('books_' . $id, config('app.cache_ttl'), function () use ($id) {
            return Book::findOrFail($id);
        }));
    })->middleware([ApiRequestLogging::class]);

    Route::get('/books', function (Request $request, Book $book) {
        $books = Cache::remember('books_p' . ($request->get('page') ?? 1), config('app.cache_ttl'), function () use ($book) {
            return $book->getAllBooks()->paginate(config('app.page_size'));
        });
        if (!$books || count($books) == 0) {
            return response(null, ResponseCode::NO_FOUND->value);
        }
        return BookResource::collection($books);
    })->middleware([ApiRequestLogging::class]);

    /*
     * authors
     */
    Route::get('/authors/{id}', function (int $id) {
        return new AuthorResource(Cache::remember('authors_' . $id, config('app.cache_ttl'), function () use ($id) {
            return Author::findOrFail($id);
        }));
    })->middleware([ApiRequestLogging::class]);

    Route::get('/authors', function (Request $request, Author $author) {
        $authors = Cache::remember('authors_p' . ($request->get('page') ?? 1), config('app.cache_ttl'), function () use ($author) {
            return $author->getAllAuthors()->paginate(config('app.page_size'));
        });
        if (!$authors || count($authors) == 0) {
            return response(null, ResponseCode::NO_FOUND->value);
        }
        return AuthorResource::collection($authors);
    })->middleware([ApiRequestLogging::class]);

