<?php

namespace App\Services\MailSenders;

use App\interfaces\MailSender;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class AmqpSender implements MailSender
{
    public function send(array $data): void
    {
        $config = config('mail.send');
        $connection = new AMQPStreamConnection($config['host'], $config['port'], $config['login'], $config['password']);

        $this->publish($data, $connection);

        $connection->close();
    }

    private function publish(array $data, AMQPStreamConnection $connection): void
    {
        $channel = $connection->channel();

        $msg = new AMQPMessage(
            json_encode($data),
            array('delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT)
        );

        $channel->basic_publish($msg, '', config('mail.mailing_routing'));

        $channel->close();
    }
}
