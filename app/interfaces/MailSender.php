<?php

namespace App\interfaces;

interface MailSender
{
    public function send(array $data): void;
}
