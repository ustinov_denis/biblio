<?php

namespace App\Models;

use App\Values\MailValue;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Mailing extends Model
{
    use HasFactory;

    public $table = 'mailing';

    public $timestamps = false;

    public $fillable = ['mail'];

    public static array $rules = [
        'mail' => ['required', 'string'],
    ];

    public function addMail(MailValue $mailValue): static|bool
    {
        $mail = static::create(['mail' => $mailValue->mail]);
        Cache::flush();

        if (!$mail) {
            return false;
        }

        return $mail;
    }

    public function deleteMail(int $id): bool
    {
        $mailItem = static::findOrFail($id);

        if ($mailItem->delete()) {
            Cache::flush();

            return true;
        }

        return false;
    }

    public function getAllMails(): Builder
    {
        return static::query()->orderBy('mail');
    }

    public function getMailList(): array
    {
        return static::query()->get()->toArray();
    }

    public function getMail(string $mail): static|null
    {
        return Mailing::where('mail', $mail)->get()->first();
    }
}
