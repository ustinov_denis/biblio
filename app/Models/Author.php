<?php

namespace App\Models;

use App\Values\AuthorValueCollection;
use App\Values\AuthorValue;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;

class Author extends Model
{
    use HasFactory;

    public $table = 'Author';

    protected $fillable = ['fio'];

    public $timestamps = false;

    public static array $rules = [
        'fio' => ['required', 'string'],
    ];

    public function books()
    {
        return $this->belongsToMany(Book::class);
    }

    public function addAuthors(AuthorValueCollection $authorCollection): Collection|false
    {
        DB::beginTransaction();
        try {
            foreach ($authorCollection->collection as $item) {
                $author = static::create($item->toArray());
                $authorIds[] = $author->id;
            }
            DB::commit();
            Cache::flush();
        } catch (\Throwable $e) {
            DB::rollBack();
            return false;
        }

        $authors = static::find($authorIds)->sortBy('fio');
        if (count($authors) > 0) {
            return $authors;
        }
        return false;
    }

    public function updateAuthor(AuthorValue $authorValue): Author|false
    {
        $result = static::findOrFail($authorValue->id)->update($authorValue->toArray());
        Cache::flush();

        if ($result) {
            return static::findOrFail($authorValue->id);
        }

        return false;
    }

    public function deleteAuthor(int $id): bool
    {
        $author = static::findOrFail($id);

        DB::beginTransaction();
        try {
            if ($author->delete()) {
                $author->books()->detach();
            }

            DB::commit();
            Cache::flush();
            return true;
        } catch (\Throwable $e) {
            DB::rollBack();
            return false;
        }
    }

    public function getAllAuthors(): Builder
    {
        return static::query()->orderBy('fio');
    }
}
