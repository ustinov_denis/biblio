<?php

namespace App\Models;

use App\interfaces\MailSender;
use App\Services\Mail;
use App\Values\BookValue;
use App\Values\BookValueCollection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;

class Book extends Model
{
    use HasFactory;

    public function __construct()
    {
    }

    public $table = 'book';

    protected $fillable = ['name', 'description', 'isbn', 'year', 'photo'];

    public $timestamps = false;

    public function authors()
    {
        return $this->belongsToMany(Author::class);
    }

    public function getImage()
    {
        if ($this->photo) {
            return url("/uploads/" . $this->photo);
        } else {
            return url("/img/default.jpeg");
        }
    }

    public function getAuthors()
    {
        return $this->authors()->get()->all();
    }

    public function addBooks(BookValueCollection $bookCollection): Collection|false
    {
        DB::beginTransaction();
        try {
            $bookIds = [];

            foreach ($bookCollection as $item) {  dd($item);
                $book = static::create($item->toArray());

                $bookIds[] = $book->id;

                $book->authors()->attach($item->authors);
            }
            DB::commit();
            Cache::flush();
        } catch (\Throwable $e) {
            DB::rollBack();
            return false;
        }

        $books = static::find($bookIds)->sortBy('id');

        if (count($books) > 0) {
            return $books;
        }
        return false;
    }

    public function updateBook(BookValue $bookValue): Book|false
    {
        $bookData = static::findOrFail($bookValue->id);

        DB::beginTransaction();
        try {
            $result = $bookData->update($bookValue->toArray());
            if (!$result) {
                DB::rollBack();
                return false;
            }

            $book = static::findOrFail($bookValue->id);
            if (!array_key_exists('authors', $bookValue->toArray())) {
                DB::commit();
                Cache::flush();
                return $book;
            }

            $book->authors()->sync($bookValue->toArray()['authors']);
            DB::commit();
            Cache::flush();
            return $book;
        } catch (\Throwable $e) {
            DB::rollBack();
            return false;
        }
    }

    public function deleteBook(int $id): bool
    {
        $book = static::findOrFail($id);

        DB::beginTransaction();
        try {
            if ($book->delete()) {
                $book->authors()->detach();
            }
            DB::commit();
            Cache::flush();
            return true;
        } catch (\Throwable $e) {
            DB::rollBack();
            return false;
        }
    }

    public function getAllBooks(): Builder
    {
        return static::query()->orderBy('id');
    }
}
