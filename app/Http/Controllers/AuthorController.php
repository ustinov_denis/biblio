<?php

namespace App\Http\Controllers;

use App\Enums\ResponseCode;
use App\Http\Requests\AuthorStoreRequest;
use App\Http\Requests\AuthorUpdateRequest;
use App\Http\Resources\AuthorResource;
use App\interfaces\MailSender;
use App\Models\Author;
use App\Values\AuthorValueCollection;
use App\Values\AuthorValue;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class AuthorController extends Controller
{
    public function __construct(private readonly MailSender $mailSender, private readonly Author $author)
    {
    }

    public function store(AuthorStoreRequest $request): Response
    {
        $request->validate($request->rules());

        $authorValues = new AuthorValueCollection($request->all());
        $authors = $this->author->addAuthors($authorValues);

        if ($authors) {
            Log::info('Автор успешно добавлен', $request->all());
            return response(AuthorResource::collection($authors), ResponseCode::CREATE->value);
        }

        return response(null, ResponseCode::SERVER_ERROR->value);
    }

    public function update(AuthorUpdateRequest $request, int $id): Response
    {
        $request->validate($request->rules());

        $authorValue = new AuthorValue($request->json()->all(), $id);
        $author = $this->author->updateAuthor($authorValue);

        if ($author) {
            Log::info('Автор успешно обновлен', [$id, $request->all()]);
            return response(new AuthorResource($author));
        }

        return response(null, ResponseCode::SERVER_ERROR->value);
    }

    public function destroy(int $id): Response
    {
        $delete = $this->author->deleteAuthor($id);

        if ($delete) {
            Log::info('Автор успешно удален', [$id]);
            return response()->noContent();
        }

        return response(null, ResponseCode::SERVER_ERROR->value);
    }
}
