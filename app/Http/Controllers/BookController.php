<?php

namespace App\Http\Controllers;

use App\Enums\ResponseCode;
use App\Http\Requests\BookStoreRequest;
use App\Http\Requests\BookUpdateRequest;
use App\interfaces\MailSender;
use App\Models\Mailing;
use App\Values\BookValue;
use App\Values\BookValueCollection;
use App\Http\Resources\BookResource;
use App\Models\Book;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class BookController extends Controller
{
    public function __construct(
        private readonly MailSender $mailSender,
        private readonly Book $book,
        private readonly Mailing $mailing
    ) {
    }

    public function store(BookStoreRequest $request): Response
    {
        $request->validate($request->rules());

        $bookValues = new BookValueCollection($request->all());
        $books = $this->book->addBooks($bookValues);

        if ($books) {
            Log::info('Книги успешно добавлены', $request->all());
            $this->sendMail();
            return response(BookResource::collection($books), ResponseCode::CREATE->value);
        }

        return response(null, ResponseCode::SERVER_ERROR->value);
    }

    public function update(BookUpdateRequest $request, int $id): Response
    {
        $request->validate($request->rules());

        $bookValue = new BookValue($request->json()->all(), $id);
        $book = $this->book->updateBook($bookValue);

        if ($book) {
            Log::info('Книга успешно обновлена', [$id, $request->all()]);
            return response(new BookResource($book));
        }

        return response(null, ResponseCode::SERVER_ERROR->value);
    }

    public function destroy(int $id): Response
    {
        $delete = $this->book->deleteBook($id);

        if ($delete) {
            Log::info('Книга успешно удалена', [$id]);
            return response()->noContent();
        }

        return response(null, ResponseCode::SERVER_ERROR->value);
    }

    private function sendMail(): void
    {
        $data = $this->mailing->getMailList();
        $this->mailSender->send($data);
    }
}
