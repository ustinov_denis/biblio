<?php

namespace App\Http\Controllers;

use App\Values\MailValue;
use App\Enums\ResponseCode;
use App\Http\Requests\MailingStoreRequest;
use App\Http\Resources\MailingResource;
use App\Models\Mailing;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class MailingController extends Controller
{
    public function __construct(private readonly Mailing $mailing)
    {
    }

    public function store(MailingStoreRequest $request): Response
    {
        $request->validate($request->rules());

        $mailValue = new MailValue($request);

        $mail = $this->mailing->addMail($mailValue);

        if ($mail) {
            Log::info('Email успешно добавлен', $request->all());
            return response(new MailingResource($mail), ResponseCode::CREATE->value);
        }

        return response(null, ResponseCode::SERVER_ERROR->value);
    }
    public function destroy(string $mail): Response
    {
        $delete = $this->mailing->deleteMail($mail);

        if ($delete) {
            Log::info('Email успешно удален', [$mail]);
            return response()->noContent();
        }

        return response(null, ResponseCode::SERVER_ERROR->value);
    }
}
