<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MailingStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'mail' => 'required|email|unique:mailing',
        ];
    }

    public function messages(): array
    {
        return [
            'required' => 'Обязательное поле :attribute',
            'unique' => ':attribute должно быть уникальным',
        ];
    }
}
