<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'year' => 'date_format:Y',
            'isbn' => 'digits_between:10,13',
        ];
    }

    public function messages(): array
    {
        return [
            'date_format' => 'Некорректный формат года',
            'digits_between' => 'Некорректный формат isbn',
        ];
    }
}
