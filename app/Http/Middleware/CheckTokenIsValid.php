<?php

namespace App\Http\Middleware;

use App\Enums\ResponseCode;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;

class CheckTokenIsValid
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if ($request->header('KEYAPI') !== config('app.keyapi')) {
            return response(null, ResponseCode::AUTH_ERROR->value);
        }

        return $next($request);
    }
}
