<?php

namespace App\Http\Middleware;

use App\Enums\ResponseCode;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;

class ApiRequestLogging
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        return $next($request);
    }

    public function terminate(Request $request, Response $response): void
    {
        if ($response->getStatusCode() == ResponseCode::SERVER_ERROR->value) {
            Log::alert('Ошибка ', [$request, $response]);
        } elseif ($response->getStatusCode() == ResponseCode::NO_FOUND->value) {
            Log::alert('Ошибка ', [$request]);
        }
    }
}
