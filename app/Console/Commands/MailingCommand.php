<?php

namespace App\Console\Commands;

use App\Mail\BookEmail;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use Illuminate\Support\Facades\Mail;

class MailingCommand extends Command
{
    protected $signature = 'rabbitmq:mailing';

    protected $description = 'RabbitMQ AmqpSender';

    public function handle()
    {
        $connection = new AMQPStreamConnection(
            'localhost',
            5672,
            'guest',
            'guest'
        );
        $channel = $connection->channel();

        $channel->queue_declare(
            'laravel_mailing',
            false,
            true,
            false,
            false
        );

        echo " [*] Waiting for messages. To exit press CTRL+C\n";

        $callback = function ($data) {
            $mails = json_decode($data->body, true);
            if (!is_array($mails) || count($mails) == 0) {
                return false;
            }

            foreach ($mails as $mail) {
                $res = Mail::to($mail['mail'])->send(new BookEmail());
                if (!$res) {
                    Log::alert('Не отправилась почта', $mail);
                }
            }

            $data->delivery_info['channel']->basic_ack($data->delivery_info['delivery_tag']);
        };

        $channel->basic_qos(null, 1, null);

        $channel->basic_consume(
            'laravel_mailing',
            '',
            false,
            false,
            false,
            false,
            $callback
        );

        while ($channel->is_consuming()) {
            $channel->wait();
        }

        $channel->close();

        return Command::SUCCESS;
    }
}
