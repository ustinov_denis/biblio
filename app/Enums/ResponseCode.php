<?php

namespace App\Enums;

enum ResponseCode: int
{
    case CREATE = 201;
    case SERVER_ERROR = 500;
    case AUTH_ERROR = 401;
    case DELETE = 204;
    case NO_FOUND = 404;
}
