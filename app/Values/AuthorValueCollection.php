<?php

namespace App\Values;

class AuthorValueCollection extends CollectionValue
{
    protected string $class = 'AuthorValue';
}
