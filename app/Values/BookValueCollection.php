<?php

namespace App\Values;

class BookValueCollection extends CollectionValue
{
    protected string $class = 'BookValue';
}
