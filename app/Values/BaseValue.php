<?php

namespace App\Values;

abstract class BaseValue
{
    abstract public function toArray(): array;
}
