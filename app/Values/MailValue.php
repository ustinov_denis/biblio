<?php

namespace App\Values;

use Illuminate\Foundation\Http\FormRequest;

class MailValue extends BaseValue
{
    public readonly string $mail;

    public function __construct(FormRequest $request)
    {
        $this->mail = $request->json()->all()['mail'];
    }

    public function toArray(): array
    {
        return [
            'mail' => $this->mail,
        ];
    }
}
