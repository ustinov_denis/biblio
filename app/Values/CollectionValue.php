<?php

namespace App\Values;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Collection;

class CollectionValue
{
    public readonly Collection $collection;

    protected string $class;

    public function __construct(array $data)
    {
        $this->collection = collect();
        $class = __NAMESPACE__ . '\\' . $this->class;

        foreach ($data as $item) {
            $objItem = new $class($item);
            $this->collection->add($objItem);
        }
    }
}
