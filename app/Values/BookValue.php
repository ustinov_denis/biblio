<?php

namespace App\Values;

use http\Client\Request;

class BookValue
{
    public readonly int|null $id;

    public readonly string|null $name;

    public readonly int|null $year;

    public readonly string|null $description;

    public readonly string|null $isbn;

    public readonly string|null $photo;

    private readonly string|null $authors;

    public function __construct(array $data, int $id = null)
    {
        $this->name = $data['name'] ?? null;

        $this->year = $data['year'] ?? null;

        $this->description = $data['description'] ?? null;

        $this->isbn = $data['isbn'] ?? null;

        $this->photo = $data['photo'] ?? null;

        $this->authors = $data['authors'] ?? null;

        $this->id = $id;
    }

    public function toArray(): array
    {
        $ret = [
        ];

        if ($this->id != null) {
            $ret['id'] = $this->id;
        }

        if ($this->name != null) {
            $ret['name'] = $this->name;
        }

        if ($this->year != null) {
            $ret['year'] = $this->year;
        }

        if ($this->description != null) {
            $ret['description'] = $this->description;
        }

        if ($this->isbn != null) {
            $ret['isbn'] = $this->isbn;
        }

        if ($this->photo != null) {
            $ret['photo'] = $this->photo;
        }

        if ($this->authors != null) {
            $ret['authors'] = $this->authors;
        }

        return $ret;
    }
}
