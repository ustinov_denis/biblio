<?php

namespace App\Values;

class AuthorValue extends BaseValue
{
    public readonly int|null $id;

    public readonly string $fio;

    public function __construct(array $data, int $id = null)
    {
        $this->fio = $data['fio'];

        $this->id = $id;
    }

    public function toArray(): array
    {
        $ret = [
            'fio' => $this->fio,
        ];

        if ($this->id != null) {
            $ret['id'] = $this->id;
        }

        return $ret;
    }
}
