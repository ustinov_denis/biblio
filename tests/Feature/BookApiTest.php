<?php

namespace Tests\Feature;

use App\Enums\ResponseCode;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class BookApiTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->artisan('migrate --env=testing');
        $this->artisan('db:seed --env=testing');
        //$this->artisan('db:seed');
        //$this->seed();
    }

    public function tearDown(): void
    {
        $this->artisan('migrate:reset --env=testing');
    }

    public function testGetBooks(): void
    {
        $response = $this->get('/books');

        $response->assertStatus(200);

        $response->assertJson(fn (AssertableJson $json) =>
            $json->whereType('data', 'array')
                ->whereType('data.0','array')
                ->whereType('data.0.name', 'string')
                ->etc()
        );

        $id = $response->original[0]->id;

        $response = $this->get('/books/' . $id);

        $response->assertStatus(200);

        $response->assertJson(fn (AssertableJson $json) =>
        $json->whereType('data', 'array')
            ->whereType('data.name', 'string')
            ->etc()
        );
    }

    public function testModifyBooks(): void
    {
        $addBookData = [
            [
                'name' => 'book' . time(),
                'isbn' => '1234567890',
                'year' => 2024,
                'authors' => [
                    2
                ]
            ]
        ];

        $response = $this->postJson('/books', $addBookData, [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'KEYAPI' => env('KEYAPI'),
        ]);

        $response->assertStatus(ResponseCode::CREATE->value);

        $id = $response->original[0]->id;

        $updateBookData = [
            [
                'name' => 'book' . time(),
                'isbn' => '1234567891',
                'year' => 2025,
                'authors' => [
                    3
                ]
            ]
        ];

        $response = $this->putJson('/books/' . $id, $updateBookData, [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'KEYAPI' => env('KEYAPI'),
        ]);

        $response->assertStatus(200);

        $response = $this->deleteJson('/books/' . $id, [] ,[
            'Content-Type' => 'application/json',
            'KEYAPI' => env('KEYAPI'),
        ]);

        $response->assertStatus(ResponseCode::DELETE->value);
    }
}
