<?php

namespace Tests\Unit;

use App\Models\Book;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Tests\TestCase;
use App\Values\BookValueCollection;
use App\Values\BookValue;

class BookTest extends TestCase
{

    public function setUp(): void
    {
        parent::setUp();
        $this->artisan('migrate --env=testing');
        $this->artisan('db:seed --env=testing');

        //$this->seed();
        $this->createApplication();
    }

    public function tearDown(): void
    {
        $this->artisan('migrate:reset --env=testing');
    }

    public function testModifyBook(): void
    {
        $book = new Book();

        $addBookData = [
            [
                'name' => 'book' . time(),
                'isbn' => '1234567890',
                'year' => '2024',
                'authors' => [
                    2
                ]
            ]
        ];

        $bookValues = new BookValueCollection($addBookData);
        $addBooks = $book->addBooks($bookValues);

        $this->assertInstanceOf(Collection::class, $addBooks, 'Добавление книг возвращает объект класса Collection');
        $this->assertInstanceOf(Book::class, $addBooks[0], 'Нулевой элемент возврата добавления книги - объект класса Book');

        $updateBookData = [
            'name' => 'book' . time(),
            'isbn' => '1234567891',
            'year' => '2025',
            'authors' => [
                3
            ]
        ];

        $updateBookValues = new BookValue($updateBookData, $addBooks[0]->id);

        $updateBooks = $book->updateBook($updateBookValues);

        $this->assertInstanceOf(Book::class, $updateBooks, 'Обновление книги возвращает объект класса Book');
        $this->assertEquals($updateBooks->id, $addBooks[0]->id, 'id изменнной книги');
        $this->assertEquals($updateBooks->name, $updateBookData['name'], 'название изменнной книги');
        $this->assertEquals($updateBooks->isbn, $updateBookData['isbn'], 'isbn изменнной книги');
        $this->assertEquals($updateBooks->year, $updateBookData['year'], 'год издания измененной книги');

        $deleteBooks = $book->deleteBook($addBooks[0]->id);
        $this->assertEquals($deleteBooks, true, 'Удаление книги успешно');
    }

    public function testGetBooks()
    {
        $book = new Book();
        $books = $book->getAllBooks()->paginate(config('app.page_size'));
        $this->assertInstanceOf(LengthAwarePaginator::class, $books, 'Первая страницы выборки книг');
        $bookOne = $books->get(0);
        $this->assertInstanceOf(Book::class, $bookOne, 'Первая книга на первой странице');

    }
}
